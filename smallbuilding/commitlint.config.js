
// 提交 连接配置

// 在提交代码（git提交）之前，对代码有一个检查，拦截
// 在2.0版本后 引入了一个新功能 ：直接拦截 git .hooks;将.git目录 更改 为指向 .husky目录

// husky 会在我们每次提交之前 都会执行 npm run test 测试一下代码；
// 配合eslink约束代码的规范

// npm run test 测试脚本

// $0 是用用来记录 提交的次数

// husky 是用来配合 eslink 使用的

// 安装 npm i lint-staged -S 用法：他会在我们执行 git 之前，校验你的代码规范

// 安装 npm install --save-dev @commitlint/config-conventional @commitlint/cli

module.exports = {
    // 使用 commitlint/config-conventional
    extends: ['@commitlint/config-conventional'],
    // 指定拦截规则
    rules: {
        'type-enum': [
            'alwys',  // 总是（每次）都要拦截
            [
                'build',
                'chore',
                'ci',
                'docs',
                'feat',
                'fix',
                'perf',
                'refactor',
                'revert',
                'style',
                'test'
            ]
        ],
        "type-case": [ // 提交的类型
            0 // 错误级别为 0
        ],
        "type-empty": [ // 
            0 // 错误级别 0
        ],
        "subject-case": [0, "never"]
    }
}