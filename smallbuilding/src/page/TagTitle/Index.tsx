import "./tagtitle.scss";
import i18next from "i18next";
import * as http from "../../api/Tag/tag";
import { useTranslation } from "react-i18next";
import * as actions from "../../actions/archives";
import React, { useEffect, useState } from "react";
import ListItem from "../../components/List/ListItem";
import { useDispatch, useSelector } from "react-redux";
import { taglist } from "../../store/action/Tage/action";
import { useNavigate, useParams } from "react-router-dom";
import { tagList } from "../../types/store/TagTitle/type";
import { GetActicleList } from "../../store/article/action";
import EmptyPage from "../../components/EmptyPage/EmptyPage";
import { get_archives_rightBottom_list } from "../../api/archives";
import { everyonclik } from "../../store/action/Tage/action"; //引入方法
import RecommendList from "../../components/RecommendList/RecommendList";

// 标签详情页
function Index() {
  const params = useParams();
  const navigate = useNavigate();
  const dispatch: any = useDispatch();
  const [list, setList] = React.useState<any>([]);
  const [tuiList, setTuiList] = React.useState([]); // 右侧 上部 推荐数据
  const [categoryTitleList, setTategoryTitleList] = useState([]); // 右侧 上部 推荐数据

  //获取文章的所有
  useEffect(() => {
    dispatch(GetActicleList());
    http.CheShi(params.value).then((res) => {
      dispatch(everyonclik(res.data.data.id));
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);

  useEffect(() => {
    dispatch(taglist());

    //获取右侧上方的数据
    http.GetTuiList().then((res) => {
      setTuiList(res.data.data);
    });

    // 右侧 categoryTitleList 数据
    get_archives_rightBottom_list().then((res) => {
      setTategoryTitleList(res.data.data);
      console.log(res.data.data);
    });
  }, [dispatch]);

  // 中间 tag的所有标签数据 获取
  const taglists = useSelector((state: any) => state.TageReducer.taglist);

  //获取文章的所有
  useEffect(() => {
    dispatch(GetActicleList());
  }, [dispatch]);

  //筛选出来的点击的数据
  const everystate = useSelector((state: any) => state.TageReducer.everyonclik);
  // 右下面的数据
  useEffect(() => {
    dispatch(actions.get_archives_bottom_LIST());
  }, [dispatch]);

  //点击每个事件，出现高亮状态
  const everytag = (id: string, item: any) => {
    console.log(id);

    //筛选作者
    dispatch(everyonclik(id));
    http.GetTagVal(item.value).then((res) => {
      setList(res.data.data[0]);
    });
  };

  const [lang, setLang] = useState(() => navigator.language); //zh-cn中文
  const { t } = useTranslation();

  return (
    <div className="tag-wrap">
      <div className="zuo">
        {/* 上面 */}
        <div className="zuos">
          {/* 国际化 */}
          国际化
          <p>{t("article")}</p>
          <button
            onClick={() => {
              const newLang = lang === "en" ? "zh-CN" : "en";
              setLang(newLang);

              //调用i18n的changeLanguage方法
              i18next.changeLanguage(newLang);
            }}
          >
            切换中英文{lang}{" "}
          </button>
          {everystate.length
            ? everystate.map((item: tagList, index: number) => {
                return (
                  <div key={index}>
                    <div className="tagitem ">
                      <p>
                        yu <span className="itemlab">{item.label}</span>{" "}
                        tagRelativeArticles <br />
                        totalSearch{" "}
                        <span className="itemlab1">
                          {item.articleCount}
                        </span>{" "}
                        piece
                      </p>
                    </div>
                  </div>
                );
              })
            : "暂无数据"}
        </div>
        {/* 中间 */}
        <div className="zuoz">
          <div className="tagTitle">
            <h3>tagTitle</h3>
            <div className="tab-box">
              {taglists && taglists.length >= 1
                ? taglists.map((item: tagList, index: number) => {
                    return (
                      <div
                        key={index}
                        onClick={() => {
                          everytag(item.id, item);
                          //跳转页面
                          navigate(`/smallbuilding/tage/${item.value}`);
                        }}
                        className={
                          params.value === item.value
                            ? "tag-tagactive"
                            : "tagactive"
                        }
                      >
                        {item.label}[{item.articleCount}]
                      </div>
                    );
                  })
                : "暂无数据"}
            </div>
          </div>
        </div>
        {/* 下面 */}
        <div className="zuox">
          {list && list.length ? (
            <ListItem data={list}></ListItem>
          ) : (
            <EmptyPage />
          )}
        </div>
      </div>
      <div className="you">
        <div className="yous">
          <div className="tagTitle">
            <RecommendList str={"recommendToReading"}>
              {tuiList &&
                tuiList.map((item: any) => {
                  return (
                    <li
                      key={item.id}
                      onClick={() => {
                        navigate("/smallbuilding/articleDetails/" + item.id, {
                          state: item,
                        });
                      }}
                    >
                      {item.title}
                    </li>
                  );
                })}
            </RecommendList>
          </div>
        </div>
        <div className="youx">
          <div className="tagTitle">
            {
              <RecommendList str={"categoryTitle"}>
                {categoryTitleList &&
                  categoryTitleList.map((item: any, index: number) => {
                    return (
                      <li
                        key={index}
                        style={{
                          animation: `example1 ${(index + 1) * 0.1}s ease-out ${
                            (index + 2) * 0.1
                          }sbackwards`,
                        }}
                        onClick={() => {
                          navigate("/smallbuilding/category/" + item.value, {
                            state: item,
                          });
                        }}
                      >
                        <span> {item.label} </span>total {item.articleCount}
                        articleCountTemplate
                        <span>{item.label}</span>
                        total&emsp;{item.articleCount}&emsp;articleCountTemplate
                      </li>
                    );
                  })}
              </RecommendList>
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default Index;
