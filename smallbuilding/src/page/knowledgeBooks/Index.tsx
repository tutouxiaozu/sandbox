import React, { useState } from "react";
import "./css/knowledge.css";
import {
  Knowledge,
  dispatch,
} from "../../types/store/Knowledge/knowledge";
import {
  useEffect,
} from "react";
import { get_Knowledge } from "../../actions/knowledge/action";
import { useDispatch, useSelector } from "react-redux";
import RecommendList from "../../components/RecommendList/RecommendList";
import KnowledgeList from "../../components/knowlegeList/index"
import {rightTop,rightBottom} from "../../api/knowledge/action"
import {useNavigate} from "react-router-dom"
import time from "../../utils/time";
// import FooterGItHub from "../../components/FooterGitHub/FooterGItHub";
// 知识小册
function Index() {
  const dispatch: dispatch = useDispatch();
  const navigate=useNavigate()
  useEffect(() => {
    dispatch(get_Knowledge());
  }, [dispatch]);
  const knowledgeList = useSelector(
    (state: Knowledge) => state.KnowledgeReducer.knowledgeList
  );
  const data:any= knowledgeList.filter((item: any) => item.cover);

  const [top,setTop]=useState([[]])
  useEffect(()=>{
    rightTop().then((res:any)=>{
      setTop(res.data.data)
    })
  },[])
  const [bottom,setBottom]=useState([[]])
  useEffect(()=>{
    rightBottom().then((res:any)=>{
      setBottom(res.data.data)
    })
  },[])
  return (
    <div className="knowledge">
      <div className="left">
        <div>
          <KnowledgeList
            data={data}
          ></KnowledgeList>
        </div>
      </div>
      <div className="right">
        <div className="top">
          <RecommendList str={"推荐阅读"}>
            {top &&
              top.map((item: any, index: number) => {
                return (
                  <li
                    key={index}
                    onClick={() =>
                      navigate("/smallbuilding/articleDetails/" + item.id, {
                        state: item,
                      })
                    }
                  >
                    <span>{item.title}</span>
                    <span>.</span>
                    <span>大约{time(item.createAt)}</span>
                  </li>
                );
              })}
          </RecommendList>
        </div>
        <div className="bottom">
          <RecommendList str={"文章分类"}>
            {bottom&&
              bottom.map((item: any, index: number) => {
                return (
                  <li
                    key={index}
                    onClick={() =>
                      navigate("/smallbuilding/tage/" + item.value, {
                        state: item,
                      })
                    }
                  >
                    <span>{item.label}</span>
                    <span>共计{item.articleCount}文章</span>
                  </li>
                );
              })}
          </RecommendList>
        </div>
      </div>
      {/* <FooterGItHub></FooterGItHub> */}
    </div>
  );
}

export default Index;