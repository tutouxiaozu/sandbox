import React from "react";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { get_Knowledge } from "../../actions/knowledge/action";
import { Knowledge, dispatch } from "../../types/store/Knowledge/knowledge";
import "./css/detail.css";
import { Breadcrumb } from "antd";
import { EyeOutlined, ShareAltOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import KnowledgeModal from "../../components/knowledgeModal/index";
// 知识小册 详情页
function Index() {
  const params = useParams();
  console.log(params, "9999");
  const dispatch: dispatch = useDispatch();
  useEffect(() => {
    dispatch(get_Knowledge());
  }, [dispatch]);
  const knowledgeList = useSelector(
    (state: Knowledge) => state.KnowledgeReducer.knowledgeList
  );
  const arr = knowledgeList.filter((item: any) => item.id === params.id)[0];
  const brr = knowledgeList.filter((item: any) => item.id !== params.id);

  const navigate = useNavigate();

  const [flag, setFlag] = useState(false);
  const close = (flag: any) => {
    setFlag(flag);
  };
  const [Item, setItem] = useState(null);
  return (
    <div className="detail">
      <div className="left">
        <div className="zz">
          <Breadcrumb>
            <Breadcrumb.Item>
              <a href="/smallbuilding/knowledgebooks">knowledgeBooks</a>/
              {arr.title}
            </Breadcrumb.Item>
          </Breadcrumb>
          <h3>{arr.title}</h3>
        </div>
        <div className="div">
          <img src={arr.cover} alt="" className="img" />
          <h3>{arr.summary}</h3>
          <p>{arr.createAt}</p>
          <button
            onClick={() =>
              navigate("/smallbuilding/articleDetails/" + arr.id, {
                state: arr,
              })
            }
          >
            startReading
          </button>
          <p>pleaseWait</p>
        </div>
      </div>
      <div className="right">
        <h3>ontheknowledge</h3>
        <div className="list">
          {brr.length
            ? brr.map((item: any, index: number) => {
                return (
                  <li
                    key={index}
                    onClick={() =>
                      navigate(`/smallbuilding/knowledgeDetails/${item.id}`)
                    }
                  >
                    <p>
                      <h3>{item.title}</h3>
                      <span>{item.createAt}</span>
                    </p>
                    <dl>
                      <dt>
                        <img src={item.cover} alt="" />
                      </dt>
                      <dd>
                        <h4>{item.summary}</h4>
                        <p>
                          <span>
                            <EyeOutlined />
                          </span>
                          <span>{item.views}</span>
                          <span
                            onClick={(e) => {
                              e.stopPropagation();
                              setFlag(true);
                              setItem(item);
                            }}
                          >
                            <ShareAltOutlined />
                            share
                          </span>
                        </p>
                      </dd>
                    </dl>
                  </li>
                );
              })
            : ""}
        </div>
        <KnowledgeModal
          showModal={flag}
          close={close}
          ModalList={Item}
        ></KnowledgeModal>
      </div>
    </div>
  );
}

export default Index;
