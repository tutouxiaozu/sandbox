import {
  GetActicleTags,
  ActicleRecommend,
  GetAllCategoryList,
} from "../../api/articleApi/actionApi";
import { ActicleListType } from "../../types/store/ArticleReducer/acticeReducer.d";
import RecommendList from "../../components/RecommendList/RecommendList";
import SwoperItem from "../../components/SwiperItem/SwiperItem";
import EmptyPage from "../../components/EmptyPage/EmptyPage";
import TabList from "../../components/TabList/TabList";
import { useDispatch, useSelector } from "react-redux";
import ListItem from "../../components/List/ListItem";
import * as action from "../../store/article/action";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import store from "../../store";
import "./index.scss";

type AppDispatch = typeof store.dispatch;

// 文章页面
function Index() {
  const navigate = useNavigate();
  const dispatch: AppDispatch = useDispatch();
  const [recommend, setRecommend] = useState([]);
  const [TagData, setTagData] = React.useState([]);
  const [category, setCategory] = useState([]);
  // 获取到的所有的数据

  useEffect(() => {
    dispatch(action.GetActicleList()); // 向仓库派发 所有文章仓库

    // 获取 tag 数据
    GetActicleTags().then((res) => {
      setTagData(res.data.data);
    });

    // 获取 推荐数据
    ActicleRecommend().then((res) => {
      setRecommend(res.data.data);
    });

    // 获取所有的tabNav数据
    GetAllCategoryList().then(
      (res: { data: { data: React.SetStateAction<never[]> } }) => {
        setCategory(res.data.data);
      }
    );
  }, [dispatch]);

  const acticleList = useSelector(
    (store: any) => store.ArticleReducer.acticleList
  );

  const swiperData = acticleList.filter((item: ActicleListType) => item.cover);

  return (
    <div className="article-wrap">
      <div className="article-wrap-left">
        <div className="article-wrap-left-swiper">
          <SwoperItem data={swiperData}></SwoperItem>
        </div>
        <div className="article-wrap-left-list">
          {/* 文章列表页面的导航渲染 */}
          <TabList data={category}></TabList>
          <div className="article-list-box">
            {recommend && recommend.length >= 1 ? (
              <ListItem data={recommend}></ListItem>
            ) : (
              <div className="EnptyBox">
                  <EmptyPage></EmptyPage>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="article-wrap-right">
        <div className="article-wrap-right-top">
          <RecommendList str={"recommendToReading"}>
            {acticleList &&
              recommend.map((item: any, index: number) => {
                return (
                  <li
                    key={item.id}
                    onClick={() => {
                      navigate("/smallbuilding/articleDetails/" + item.id, {
                        state: item,
                      });
                    }}
                  >
                    <span>{item.title}</span>
                    &nbsp;
                    {/* <span>✿</span> */}
                    &nbsp;
                    <span>
                      {Math.floor(
                        (new Date().getTime() -
                          new Date(item.publishAt).getTime()) /
                          (24 * 60 * 2400)
                      )}
                      天前
                    </span>
                  </li>
                );
              })}
          </RecommendList>
        </div>
        <div className="article-wrap-right-bottom">
          <RecommendList str={"recommendToReading"}>
            {TagData &&
              TagData.map((item: any, index: number) => {
                return (
                  <span
                    key={item.id}
                    onClick={() => {
                      navigate("/smallbuilding/tage/" + item.value, {
                        state: item,
                      });
                    }}
                  >
                    {item.label}[{item.articleCount}]
                  </span>
                );
              })}
          </RecommendList>
        </div>
      </div>
    </div>
  );
}

export default Index;
