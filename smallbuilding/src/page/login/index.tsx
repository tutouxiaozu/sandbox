import React, { useState, useEffect } from "react";
import i18n from "../../i18next/index";
import { useTranslation } from "react-i18next";
import "../../style/theme.css";

const isLight = () => {
  const currentDate = new Date();
  // 获取的小时 作比较
  return currentDate.getHours() > 6 && currentDate.getHours() < 19;
};

function Index() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [lang, setLang] = useState(() => navigator.language);

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { t } = useTranslation();
  const [theme, setTheme] = useState(() => (isLight() ? "白" : "黑"));

  // console.log(theme);
  
  useEffect(() => {
    // 更改主题 更改根节点的类名;
    document.documentElement.className = theme === "白" ? "light" : "dark";
  }, [theme]);

  return (
    <div>
      <p>{t("文章")}</p>
      <button
        onClick={() => {
          const newLang = lang === "en" ? "zh-CN" : "en";
          setLang(newLang);
          // 然后调用 i18next 的 changeLangage 方法;
          i18n.changeLanguage(newLang);
        }}
      >
        切换{lang}
      </button>

      <button
        onClick={() => {
          setTheme(theme === "白" ? "黑" : "白");
        }}
      >
        {theme}
      </button>
    </div>
  );
}

export default Index;
