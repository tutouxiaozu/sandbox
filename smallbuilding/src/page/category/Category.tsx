import "./category.scss";
import React from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import ListItem from "../../components/List/ListItem";
import * as http from "../../api/articleApi/actionApi";
import EmptyPage from "../../components/EmptyPage/EmptyPage";
import RecommendList from "../../components/RecommendList/RecommendList";

function Category() {
  const params = useParams();
  const navigate = useNavigate();
  const [NavList, setNavList] = React.useState([]);
  const [TagData, setTagData] = React.useState([]);
  const [recommend, setRecommend] = React.useState([]);
  const [CategoryList,setCategoryList]  = React.useState([]);
  const [CategoryTitle,setCategoryTitle]  = React.useState('');
  const [CategoryListData,setCategoryListData] = React.useState([]);


  React.useEffect(() => {
    // 定义 路由 value的值
    const val = params.value;

    // 获取 导航的数据
    http.GetAllCategoryList().then((res) => {
      setNavList(res.data.data);
    });
    // 获取 tag 数据
    http.GetActicleTags().then((res) => {
      setTagData(res.data.data);
    });

    // 获取 推荐数据
    http.ActicleRecommend().then((res) => {
      setRecommend(res.data.data);
    });

    // 获取作者信息
    http.GetCategoryList(val).then(res=>{
      setCategoryList(res.data.data)
      setCategoryListData(res.data.data[0])
    })

    http.GetCategory(val).then(res=>{
      setCategoryTitle(res.data.data.label)
    })
    
  }, [params]);

  return (
    <div className="category-wrap">
      <div className="category-wrap-left">
        <div className="category-wrap-left-information">
          <p>
            <b>{CategoryTitle}</b>
            <span>categoryArticle</span>
          </p>
          <p>
            <span>totalSearch </span>
            <b>{CategoryList[1]}</b>
            <span>piece</span>
          </p>
        </div>
        <div className="category-wrap-left-content">
          <div className="category-wrap-left-content-title">
            {NavList &&
              NavList.map((item: any) => {
                return (
                  <span className={params.value === item.value ? "title-item" : ""} key={item.id}  onClick={()=>{
                    navigate('/smallbuilding/category/'+item.value)
                  }}>
                    {item.label}
                  </span>
                );
              })}
          </div>
          <div className="category-wrap-left-content-list">
            <div>
              {CategoryList && CategoryListData.length >= 1 ? (
                <ListItem data={CategoryList[0]}></ListItem>
              ) : (
                <EmptyPage></EmptyPage>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="category-wrap-right">
        <div className="category-wrap-right-top">
          <RecommendList str={"recommendToReading"}>
            {recommend &&
              recommend.map((item: any, index: number) => {
                return (
                  <li
                    key={index}
                    onClick={() => {
                      navigate("/smallbuilding/articleDetails/" + item.id, {
                        state: item,
                      });
                    }}
                  >
                    <span>{item.title}</span>
                    &nbsp;
                     &nbsp;
                    <span>
                      {Math.floor(
                        (new Date().getTime() -
                          new Date(item.publishAt).getTime()) /
                          (24 * 60 * 2400)
                      )}
                      天前
                    </span>
                  </li>
                );
              })}
          </RecommendList>
        </div>
        <div className="category-wrap-right-bottom">
          <RecommendList str={"recommendToReading"}>
            {TagData &&
              TagData.map((item: any, index: number) => {
                return (
                  <span
                    key={index}
                    onClick={() => {
                      navigate("/smallbuilding/tage/" + item.value, {
                        state: item,
                      });
                    }}
                  >
                    {item.label}[{item.articleCount}]
                  </span>
                );
              })}
          </RecommendList>
        </div>
      </div>
    </div>
  );
}

export default Category;
