import "./details.scss";
import time from "../../utils/time";
import { QRCodeSVG } from "qrcode.react";
// import Comment from "../../components/Comment";
import * as http from "../../api/article/action";
import { useEffect, useRef, useState } from "react";
// import CommentList from "../../components/CommentList";
import { useParams, useLocation } from "react-router-dom";
import EmptyPage from "../../components/EmptyPage/EmptyPage";
import { Badge, Modal, Anchor,Button, Form, Input } from "antd";
// import { Pagination} from "antd";
import RecommendList from "../../components/RecommendList/RecommendList";
import PayBulletFrame from "../../components/PayBulletFrame/PayBulletFrame";
import { ShareAltOutlined,HeartOutlined,MessageFilled,HeartFilled } from "@ant-design/icons";

import CommentBull from "../../components/CommentBull/CommentBull";

const { Link } = Anchor;

// 文章详情页
function Index() {
  const loaction: any = useLocation();
  const [listItem, setListItem] = useState([]); // 
  // const [commentItem, setCommentItem] = useState([]); // 评论数据
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpens, setIsModalOpens] = useState(false);
  const [MoarkHtml, setMoarkHtml] = useState({}); //渲染html
  const [MoarkToc, setMoarkToc] = useState([]); //楼层导航
  const paramsId = useParams(); //拿到传参过来的数据的id
  const [loactionItem,setloactionItem] = useState<any>({});
  const [ItemPay, setItemPay] = useState(null);
  const [flagPay, setFalgPay] = useState(false);

  useEffect(() => {

    setloactionItem(loaction.state)
    console.log(loaction.state,paramsId,'loactionItem');
    
    http.get_list().then((res: any) => {
      setListItem(res.data.data);
    });
    http.GetHtml(paramsId.id).then((res) => {
      setMoarkHtml(res.data.html);
      let par = res.data.data.toc;
      setMoarkToc(JSON.parse(par));
    });
    let htmlDom: any = document.getElementById("markedHtml");

    loactionItem
      ? (htmlDom.innerHTML = loactionItem.html)
      : (htmlDom.innerHTML = <EmptyPage></EmptyPage>);
    // http.GetComment().then((res: any) => {
    //   setCommentItem(res.data.data[0]);
    // });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [MoarkHtml, paramsId.id]);

  // 知识付费 列表页面 跳转到详情页 时候 监听的事件-------------------------------------
  useEffect(()=>{
    if (!loactionItem.isPay&&loactionItem.totalAmount&&loactionItem.totalAmount!==null) {
      // console.log(loaction.state.totalAmount,'loaction.state');
      setFalgPay(true);
      setItemPay(loaction.state);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[loactionItem,flagPay])

  const clonePay = (flag: any) => {
    setFalgPay(flag);
  };

  const showModals = () => {
    setIsModalOpens(false);
  };
  const handleOks = () => {
    setIsModalOpens(false);
  };
  const handleCancels = () => {
    setIsModalOpens(false);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const showModal = () => {
    setIsModalOpen(true);
  };
  const onFinish = (values: any) => {
    console.log("Success:", values);
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  const [show, setShow] = useState(false);
  const [shows, setShows] = useState(false);


  // 点击 a 标签,跳转锚点 防止跳转页面
  (function () {
    var allJump: any = [];
    var allA = document.querySelectorAll("a"); // 获取所有a标签
    allA.forEach((ele) => {
      if (ele.href.search("#") > 12) {
        allJump.push(ele); // 判断a标签是否含有#
      }
    });
    allJump.forEach((ele: any) => {
      ele.addEventListener("click", (e: any) => {
        // 首先解析出href
        e.preventDefault(); // 这个非常关键
        var jumpTarget = ele.href.split("#")[1];
        if (jumpTarget) {
          var targaetEle: any = document.querySelector("#" + jumpTarget);
          let h: any = targaetEle.offsetTop;
          window.scrollTo(0, h);
        } else {
          window.scrollTo(0, 0);
        }
      });
    });
  }.call(window));

  // 右侧 楼层导航
  const konwledge: any = useRef(null)
  const scrollH: any = useRef(null)
  const tagList = ["h1", "h2", "h3"]
  const [ind, setInd] = useState(0)
  const scrollHandle = () => {
    const scrollY = Math.ceil(document.documentElement.scrollTop);
    // eslint-disable-next-line array-callback-return
    scrollH.current.map((item: any, index: any) => {
      if (scrollY >= item && scrollY < scrollH.current[index + 1]) {
        setInd(index);
      }
    })
  }

  useEffect(() => {
    const TNodes = [...konwledge.current.children].filter(item => {
      return tagList.includes(item.nodeName.toLowerCase())
    })
    const lastN = TNodes[TNodes.length - 1]
    scrollH.current = TNodes.map(item => item.offsetTop)

    lastN && scrollH.current.push(lastN.offsetTop + lastN.offsetHeight)
    window.addEventListener("scroll", scrollHandle, false);
    return () => {
      window.removeEventListener("scroll", scrollHandle, false);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [MoarkHtml])

  const changeindex = (ind: any) => {
    setInd(ind)
    document.documentElement.scrollTop = scrollH.current[ind]
  }

  // 楼层 
  // const [pageSize, setPageSize] = useState(1)
  // const [limit] = useState(3)
  // const pagincomments = commentItem.slice((pageSize - 1) * limit, pageSize * limit)

  return (
    <div className="Details"  >
      <div className="DetailsLefts">
        <div className="DetailsLeft">
          <img className="imgg" src={loactionItem.cover} alt="" />
          <h1>{loactionItem.title}</h1>
          <div id="markedHtml" ref={konwledge}></div>
        </div>
        <div className="H3">
          <h3>comment</h3>
        </div>
        {/* 发布评论 */}
        <div id="hee">
          {/* eslint-disable-next-line jsx-a11y/anchor-has-content */}
          <a href="#hee"></a>
          <div onClick={showModals} className="fa">
            {/* <textarea value={'11111'}></textarea> */}
            {/* <button className="Fa">发布</button> */}
            <CommentBull ></CommentBull>
          </div>
        </div>
        <div>
          {/* <Comment deng={commentItem} commentItem={pagincomments}></Comment> */}
        </div>

        <Modal
          okText="commentNamespace.userLnfoCancel"
          title="commentNamespace.userInfoTitle"
          open={isModalOpens}
          onOk={handleOks}
          onCancel={handleCancels}
        >
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="commentNamespace.userInfoName"
              name="commentNamespace.userInfoName"
              rules={[
                {
                  required: true,
                  message: "commentNamespace.userInfoNameValidMsg",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="commentNamespace.userInfoEmail"
              name="commentNamespace.userInfoEmail"
              rules={[
                {
                  required: true,
                  message: "commentNamespace.userInfoEmailValidMsg",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </div>
      <div className="DetailsRight">
        <div className="DetailsTop">
          <RecommendList str={"recommendToReading"}>
            {listItem &&
              listItem.map((item: any) => {
                return (
                  <li key={item.id}>
                    {item.title} {time(item.createAt)}
                  </li>
                );
              })}
          </RecommendList>
        </div>
        <div className="DetailsBottom">
          {MoarkToc &&
            MoarkToc.map((item: any, index: any) => {
              //
              return (
                <div
                  onClick={() => changeindex(index)}
                  className={index === ind ? "activeInd" : ""}
                  key={item.id}
                >
                  {item.text}
                </div>
              );
            })}
        </div>
      </div>
      <div className="ding">
        <div
          className="mm"
          onClick={() => {
            setShow(!show);
          }}
        >
          <Badge offset={[10, -10]} count={shows ? 1 : 0}>
            {show ? (
              <HeartFilled className="a1" onClick={() => setShows(!shows)} />
            ) : (
              <HeartOutlined
                className="a1"
                onClick={() => setShows(!shows)}
              ></HeartOutlined>
            )}
          </Badge>
        </div>

        <div className="pinglun">
          <Anchor bounds={10} affix={false}>
            <Link
              href="#hee"
              title={
                <MessageFilled className="a1" />
              }
            ></Link>
          </Anchor>
        </div>

        <div className="mm">
          <ShareAltOutlined onClick={showModal} className="a1" />
        </div>
        <Modal
          okText="下载"
          cancelText="关闭"
          title="Basic Modal"
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          className="c1"
        >
          <img
            className="imgs"
            style={{ width: "100%" }}
            src={loactionItem.cover}
            alt=""
          />
          <h1>{loactionItem.title}</h1>
          <dl className="dl">
            <dt style={{ float: "left", paddingRight: "20px" }}>
              <dl className="dl">
                <dt style={{ float: "left", paddingRight: "20px" }}>
                  <QRCodeSVG value="https://reactjs.org/" />,
                </dt>
                <dd style={{ paddingTop: "20px" }}>识别二维码查看文章</dd>
                <dd style={{ paddingTop: "28px" }}>原文分享自ikun</dd>
              </dl>
            </dt>
            <dd style={{ paddingTop: "20px" }}>识别二维码查看文章</dd>
            <dd style={{ paddingTop: "28px" }}>原文分享自ikun</dd>
          </dl>
        </Modal>
      </div>
      <PayBulletFrame
        Item={ItemPay}
        flag={flagPay}
        clone={clonePay}
      ></PayBulletFrame>
    </div>
  );
}
export default Index;
