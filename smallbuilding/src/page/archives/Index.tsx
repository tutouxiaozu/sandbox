import React, { useEffect } from "react";
import "./index.scss";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../../actions/archives";
import store from "../../store";
import time from "../../utils/time"
import RecommendList from "../../components/RecommendList/RecommendList"
import { useTranslation } from "react-i18next"
import ArchivesList from "../../components/ArchivesList";
type AppDispatch = typeof store.dispatch;
type AppState = any;
// 档案
function Index(props: any) {
  const dispatch: AppDispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  // 文章数据
  useEffect(() => {
    dispatch(actions.get_archives_LIST());
  }, [dispatch]);
  useEffect(() => {
    dispatch(actions.get_archives_right_LIST());
  }, [dispatch]);
  useEffect(() => {
    dispatch(actions.get_archives_bottom_LIST());
  }, [dispatch]);
  const list = useSelector((state: AppState) => {
    return state.ArchivesReducer.list;
  });

  const right_top_list = useSelector((state: AppState) => {
    return state.ArchivesReducer.Right_Top_list;
  });
  const right_bottom_list = useSelector((state: AppState) => {
    return state.ArchivesReducer.Bottom_list;
  });
  let archivesList = JSON.parse(JSON.stringify(list));
  let arr: AppState = Object.values(archivesList)[0];
  let listitem: AppState = arr ? Object.values(arr)[0] : "";
  return (
    <div className="ArchivesBox">
      <div className="leftBox">
        <div className="topBox">
          <p className="text_one">{t('archives')}</p>
          <p className="text_tow">
            {t("total")} <span>{listitem.length ? listitem.length : 0}</span>{t('piece')}{" "}
          </p>
        </div>
        <div className="bottomBox">
          <p className="textOne">2022</p>
          <p className="textTwo">{t('September')}</p>
          <ul>
            <ArchivesList listitem={listitem}></ArchivesList>
          </ul>
        </div>
      </div>
      <div className="rightBox">
        <div className="right_TopBox">
          {/* <h2>recommendToReading</h2> */}
          <RecommendList str={"recommendToReading"}>
            {right_top_list &&
              right_top_list.map((item: AppState, index: number) => {
                return (
                  <li
                    style={{ animation: `example1 ${(index + 1) * 0.1}s ease-out ${(index + 2) * 0.1}s backwards` }}
                    key={index} onClick={() => {
                      navigate("/smallbuilding/articleDetails/" + item.id, { state: item })
                    }}>
                    {item.title} · {time(item.createAt)}
                  </li>
                );
              })}
          </RecommendList>
        </div>
        <div className="right_BottomBox">
          <RecommendList str={"recommendToReading"}>
            {right_bottom_list &&
              right_bottom_list.map((item: AppState, index: number) => {
                return (
                  <li key={index}
                    style={{ animation: `example1 ${(index + 1) * 0.1}s ease-out ${(index + 2) * 0.1}s backwards` }}
                    onClick={() => {
                      // navigate("/smallbuilding/articleDetails/" + item.id, { state: item })
                    }}>
                    <span> {item.label} </span>{t('total')} {item.articleCount} {t('articleCountTemplate')}
                  </li>
                );
              })
            }
          </RecommendList>
        </div>
      </div>
    </div>
  );
}

export default Index;