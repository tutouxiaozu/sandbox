import "./home.scss";
import { Layout , Anchor} from "antd";
import React, { useState } from "react";
import { Outlet } from "react-router-dom";
import BaseTop from "../../components/baseTop";
import PcHeader from "../../components/PcHeader/PcHeader";
import SearchPage from "../../components/Searchpage/index";
import { HomeSconedPath } from "../../router/RouterConfig";
import style from "../../components/style/style.module.css";
import MoveHeader from "../../components/MoveHeader/MoveHeader";
import MoveBullet from "../../components/MoveBullet/MoveBullet";
import FooterGItHub from "../../components/FooterGitHub/FooterGItHub";

const { Content } = Layout;

// 一级路由页面
function Home() {
  const [show, setShow] = useState(false);
  const [routes, setRoutes] = useState([]);
  const [current, setCurrent] = useState("one");
  const [titleSvg, setTitleSvg] = useState(true);
  const [headerFlag, setHeaderFlag] = useState(true);
  const [clientPx, setClientPx] = useState<null | number>(null);

  // 获取 当前页面地址栏的地址；
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const urlParams = new URL(window.location.href);

  // 搜索按钮点击 出现搜索弹框事件；
  const SearchBtn = () => {
    setShow(true);
  };

  // 点击 移动端得更多 标签出现弹框
  const MoreUtilsClick = (state: boolean) => {
    setTitleSvg(state);
  };
  // 点击 移动端得更多 弹框消失
  const MoreUtilsCloseClick = (state: boolean) => {
    setTitleSvg(state);
  };

  // 点击移动 中的路由 点击关闭弹框
  const MoveTitleSetCurrent = (state: boolean) => {
    setTitleSvg(state);
  };

  React.useEffect(() => {
    // 获取当前的路由地址
    const pathname = urlParams?.pathname;

    // 每次刷新的时候 将地址路由与相应导航对应上,用于让头部路由显示高亮
    setCurrent(pathname);
    // 获取路由 渲染得数据

    const routesList: any = HomeSconedPath.filter((item: any) => item.meta);
    setRoutes(routesList);

    // 进去页面设置当前也页面可是宽度
    setClientPx(document.documentElement.clientWidth);
    if (document.documentElement.clientWidth <= 767) {
      setHeaderFlag(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [clientPx, current]);

  // 点击头部导航部分 改变颜色事件
  const PcTitleSetCurrent = (value: string) => {
    setCurrent(value);
  };

  // 当页面移动的时候,改变改变渲染组件的状态
  window.onresize = function () {
    if (document.documentElement.clientWidth <= 767) {
      setHeaderFlag(false);
    } else {
      setHeaderFlag(true);
    }
  };

  return (
    <div className={style.wrap}>
      <div className="home-wrap">
        <Layout className="layout">
          <Anchor>
            {headerFlag ? (
              <PcHeader
                routes={routes}
                current={current}
                PcTitleSetCurrent={PcTitleSetCurrent}
                SearchBtn={SearchBtn}
              ></PcHeader>
            ) : (
              <MoveHeader
                titleSvg={titleSvg}
                MoreUtilsClick={MoreUtilsClick}
                MoreUtilsCloseClick={MoreUtilsCloseClick}
              ></MoveHeader>
            )}
          </Anchor>
          {!titleSvg ? (
            <MoveBullet
              routes={routes}
              current={current}
              MoveTitleSetCurrent={MoveTitleSetCurrent}
              SearchBtn={SearchBtn}
            ></MoveBullet>
          ) : (
            ""
          )}

          <Content>
            <div className="site-layout-content">
              <Outlet></Outlet>
            </div>
          </Content>
          <FooterGItHub></FooterGItHub>
        </Layout>
        {/* 搜索弹框 */}
        <SearchPage show={show} setShow={setShow}></SearchPage>
        <BaseTop></BaseTop>
      </div>
    </div>
  );
}

export default Home;
