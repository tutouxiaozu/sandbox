
export interface State {
    listItem: ListItem[],
    listsItem: ListsItem[],
}
export interface ListItem {
    title: string
    id: string
    flag?: boolean
}
export interface ListsItem {
    title: string
}
export interface Actions {
    type: string,
    payload?: string | number | boolean | object | Array
}
export type AppDispatch = typeof store.dispat
export type AppDispatchs = typeof store.dispatchs
