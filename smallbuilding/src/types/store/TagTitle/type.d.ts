// import store from "../../../store/reducer/TageReducer/index";

import store from "../../../store";

export type taglist = {
  taglist: Array<tagList>;
};

export type tagList = {
  articleCount: string;
  createAt: string;
  id: string;
  label: string;
  updateAt: string;
  value: string;
};


//绑定仓库
export type dispatch = typeof store.dispatch