import store from "../../../store"

export interface Knowledge{
    knowledgeList:[],
    KnowledgeReducer:any,
    // clist:[]
}
export interface knowledgeList{
    status:string,
    id:string,
    cover?:string,
    title:string,
    summary:string,
    views:number,
    createAt:string
}
// export interface clist{
//     id:string,
//     value:string,
//     label:string
// }
export type dispatch=typeof store.dispatch