
// 文章仓库所有数据的数据类型校验
export interface ActicleReducerTypes {
    acticleList: ActicleListType[],
}

// 文章数据的类型校验
export interface ActicleListType {
    category: {
        id: string,
        label: string,
        value: string,
        createAt: string,
        updateAt: string,
        articles?: string[]
    },
    id: string,
    content: string,
    cover: string,
    createAt: string,
    html: string,
    isCommentable: boolean,
    likes: number,
    password:string,
    needPasswords: boolean,
    publishAt: string,
    status: string,
    summary: string,
    tags: string[],
    title: string,
    toc: string,
    updateAt: string,
    views:number,
    likes: number,
    isRecommended:boolean,
    totalAmount:string,
    isPay:boolean,
}

// 获取文章数据的派发方法 类型校验
// export const GetActicleList = 'GetActicleList';
// type GetActicleList = typeof GetActicleList
export interface GetActicleListType {
    type: string,
    payload: ActicleListType[]
}


// 文章仓库所有的数据类型校验
export type AllActicleActionType = GetActicleListType