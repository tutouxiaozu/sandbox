
// 引入懒加载
import { lazy } from 'react';

export const HomeSconedPath: any[] = [
    {
        path: '/smallbuilding/article',
        name: 'article',
        component: lazy(() => import('../page/article/Index')),
        meta: {
            isNav: true
        }
    },
    {
        path: '/smallbuilding/archives',
        name: 'archives',
        component: lazy(() => import('../page/archives/Index')),
        meta: {
            isNav: true
        }
    },
    {
        path: '/smallbuilding/knowledgebooks',
        name: 'knowledgeBooks',
        component: lazy(() => import('../page/knowledgeBooks/Index')),
        meta: {
            isNav: true
        }
    },
    {
        path: '/smallbuilding/tage/:value',
        name: '标签详情页',
        component: lazy(() => import('../page/TagTitle/Index')),
    },
    {
        path: '/smallbuilding/articleDetails/:id',
        name: '文章详情页',
        component: lazy(() => import('../page/articleDetails/Index')),
    },
    {
        path: '/smallbuilding/knowledgeDetails/:id',
        name: '知识小册详情页',
        component: lazy(() => import('../page/knowledgeDetails/Index')),
    },
    {
        path: '/smallbuilding/category/:value',
        name: '作者详情页',
        component: lazy(() => import('../page/category/Category')),
    },
    {
        path: '/smallbuilding',
        to: '/smallbuilding/article'
    },
]

const routes: any[] = [
    {
        path: '/smallbuilding',
        name: '首页',
        component: lazy(() => import('../page/home/Home')),
        children: HomeSconedPath
    },
    {
        path: '/login',
        name: 'Login',
        component: lazy(() => import('../page/login/index'))
    },
    {
        path: "/",
        to: "/smallbuilding"
    }
]

export default routes