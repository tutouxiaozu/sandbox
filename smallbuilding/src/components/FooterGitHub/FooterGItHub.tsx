import { GithubOutlined, CopyrightOutlined } from "@ant-design/icons";
import "./footergithub.scss";

// 底部 GIT 公共组件
function FooterGItHub() {
  return (
    <div className="footer-github-wrap">
      <div>
        <GithubOutlined
          className="footer-github"
          onClick={() => (window.location.href = "https://github.com/")}
        />
      </div>
      <div>
        <span>
          Copyright
          <CopyrightOutlined className="footer-CopyrightOutlined" />
          2022 Designed by Fantasticit.
        </span>
      </div>
    </div>
  );
}

export default FooterGItHub;
