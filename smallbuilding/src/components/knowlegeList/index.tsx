import React, { useState } from 'react'
import { EyeOutlined, ShareAltOutlined } from "@ant-design/icons";
// import {
//     knowledgeList,
//     // Knowledge,
//     // dispatch,
//   } from "../../types/store/Knowledge/knowledge";
import { useNavigate } from "react-router-dom";
import time from "../../utils/time"
import KnowledgeModal from "../knowledgeModal/index"
import { useTranslation } from "react-i18next";

interface propsType {
  data: any,
}
function Index(props: propsType) {
  const { data } = props
  const { t } = useTranslation()
  const navigate = useNavigate();
  const [flag, setFlag] = useState(false)
  const close = (flag: any) => {
    setFlag(flag)
  }
  const [Item, setItem] = useState(null)
  return (
    <div>
      {
        data && data.map((item: any, index: number) => {
          return (
            <li
              key={index}
              onClick={() =>
                navigate(`/smallbuilding/knowledgeDetails/${item.id}`)
              }
            >
              <p>
                <p className='left-title'>{item.title}</p>
                <span>{time(item.createAt)}</span>
              </p>
              <dl>
                <dt>
                  <img src={item.cover} alt="" />
                </dt>
                <dd>
                  <h4>{item.summary}</h4>
                  <div className='konwlegeList-item-svg'>
                    <span>
                      <EyeOutlined />
                    </span>
                    <span>{item.views}</span>
                    <span
                      onClick={(e) => {
                        e.stopPropagation();
                        setFlag(true);
                        setItem(item)
                      }}
                    >
                      <ShareAltOutlined />
                      {t("share")}
                    </span>
                  </div>
                </dd>
              </dl>
            </li>
          );
        })
      }
      <KnowledgeModal
        showModal={flag}
        close={close}
        ModalList={Item}
      ></KnowledgeModal>
    </div>
  )
}

export default Index