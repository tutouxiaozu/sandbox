import { useRef , useState, useEffect } from "react";

function CanvasPoster(props: any) {
  let { ItemData } = props;
  console.log(ItemData, "下载图片的数据,单条数据");

  const [datas, setData] = useState<any>({});

  // 异步获取 单条的数据
  const GetItemData = () => {
    return new Promise((resolve, reject) => {
      resolve(ItemData);
    });
  };

  // 结构并将 单条的数据 赋值给datas
  const GetHttp = async () => {
    const { data }:any = await GetItemData();
    setData(data);
  };

  // 监听 当datas发生改变的时候 重新赋值，调用 getHttp事件
  useEffect(()=>{
    GetHttp()
  },[datas])

  // 获取 canvas的dom
  const canvasDom = useRef(null)

  return <div>
      <canvas ref={canvasDom}></canvas>
      {/* <a href="/" download={`${datas.title}.png`}>下载</a> */}
      <a href="/">下载</a>
  </div>;
}

export default CanvasPoster;
