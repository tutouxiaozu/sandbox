import { useState, useMemo } from "react"
import { Form, Input, Button} from "antd"
import { useParams, useLocation } from "react-router-dom"
import { postComment } from '../../api/article/action'
import 'react-vant/lib/index.css';
import { Toast } from 'react-vant';
const { TextArea } = Input;

const CommentTop = ({ isChildren = false, onClose = () => { }, placeholder = '请输入评论内容(支持 Markdown)', parentData = {} }) => {
  const { id } = useParams();
  console.log(id);

  const location = useLocation();
  const [value, setvalue] = useState('');

  const handleClickBQ = (e: any) => {
    if (e.target.nodeName === "LI") {
      setvalue((val) => {
        return val += e.target.innerHTML
      })
    }
  }
  const commentParamsData = useMemo(() => {

    let commonData = {
      content: value,
      // email,
      hostId: id || '8447f9e5-e11e-49ac-a0a6-452648cb3704',
      // name,
      url: location.pathname
    }
    console.log(commonData.hostId, '111');

    if (isChildren) { // 子级评论
      commonData = {
        ...commonData,
        ...parentData
      }
    }
    return commonData
  }, [value, id, parentData, location]);


  const onChange = (e: any) => {
    setvalue(e.target.value);
  }
  const onSubmit = () => {
    console.log(commentParamsData);
    // 向后端发送数据
    postComment(commentParamsData);

    Toast.success('请耐心等待后台审核');

  }


  return (
    <div className="commentTop" onClick={handleClickBQ}>
      <Form.Item>
        <TextArea rows={4} onChange={onChange} value={value} placeholder={placeholder} />
      </Form.Item>
      <Form.Item>
        {
          isChildren && (
            <Button htmlType="button" onClick={onClose}>
              收起
            </Button>
          )
        }
        <Button htmlType="submit" onClick={onSubmit} type="primary">
          发表
        </Button>
      </Form.Item>

    </div>
  );
}


export default CommentTop;


