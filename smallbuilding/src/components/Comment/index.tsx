// export default Index;
import { Comment, Tooltip } from 'antd';
import { useCallback } from 'react';
import "./index.scss"
function Comments(props: any) {
    let { commentItem, item } = props;
    const deng:any=commentItem
    const colors: any = {};
    const getNum = (min: number, max: number) => Math.floor(Math.random() * (max - min) + 1);
    const getBackground = useCallback((name: string | number) => {
        return colors[name] ? colors[name] : colors[name] = `rgb(${getNum(0, 255)},${getNum(0, 255)},${getNum(0, 255)})`;
    }, [item])
    return (
        <div className="comments">
            {
                deng && deng.map((item: any) => {
                    return <ul className='commentList' key={item.id}>
                        <li>{item.name}</li>
                        <li>{item.hostId}</li>
                        <Comment
                            //   actions={actions}
                            author={item.name}
                            avatar={<span className='author' style={{ width: "50px", height: "50px", background: getBackground(item.name) }} >{item.name.slice(0, 1)}</span>}
                            content={<p>{item.content}</p>}
                            datetime={<Tooltip title="2016-11-22 11:22:33"><span>{item.createAt}</span></Tooltip>}
                        />
                        
                    </ul>
                })
            }
        </div>

    );
};

export default Comments;



// import { Comment, Pagination } from 'antd';
// import { useState, useEffect, useCallback } from "react"
// import {useParams} from 'react-router-dom'
// import { postComment } from "../../api/article/action";
// // import { formatTime } from '@/utils/utils';
// // import CommentList from "../CommentList/index";

// const colors:any = {};

// const CommentItem = ({ item }:any) => {
    

//     const [headerOpen, setHeaderOpen] = useState(false);

//     const getNum = (min:any, max:any) => Math.floor(Math.random() * (max - min) + 1);
//     const getBackground = useCallback((name:any) => {
//         return colors[name] ? colors[name] : colors[name] = `rgb(${getNum(0, 255)},${getNum(0, 255)},${getNum(0, 255)})`;
//     }, [item])

//     const handleClick = useCallback((item:any) => () => handleChangeHeaderOpen(item), []);

//     const handleChangeHeaderOpen = (item:any) => {
//         console.log(item);
//         setHeaderOpen(!headerOpen);
//     }

//     return (
//         <Comment
//             key={item.id}
//             author={<span>{item.name}</span>}
//             avatar={<span className='author' style={{ background: getBackground(item.name) }} >{item.name.slice(0, 1)}</span>}
//             content={<span>{item.content}</span>}

//             actions={[
//                 <span>{item.userAgent}</span>,
//                 // <span>大约 {formatTime(item.createAt)}</span>,
//                 <span onClick={handleClick(item)}>回复</span>
//             ]}

//         >
//             {/* {headerOpen && <CommentList parentData={{ parentCommentId: item.id, replyUserEmail: item.email, replyUserName: item.name }} isChildren={true} onClose={handleChangeHeaderOpen} placeholder={`回复 ${item.name}`} />}

//             {
//                 Array.isArray(item.children) ? item.children.map((item: { id: any; }) => (<CommentItem item={item} key={item.id} />)) : null
//             } */}
//         </Comment>
//     )
// }

// const CommentList = () => {

//     const {id} = useParams();

//     const [commentList, setCommentList] = useState([]);
//     const [total, setTotal] = useState(0);
//     const [pageOption, setPageOption] = useState({
//         page: 1,
//         pageSize: 6
//     })
//     const init = async () => {
//         const { data } = await postComment({
//             hostId: id || '8447f9e5-e11e-49ac-a0a6-452648cb3704',
//             params: pageOption
//         })
//         setCommentList(data[0])
//         setTotal(data[1]);
//     }

//     useEffect(() => {
//         init();
//     }, [pageOption])

//     const handleCommentListChange = (page:any, pageSize:any) => {
//         setPageOption({
//             page,
//             pageSize
//         })
//     }

//     return (
//         <div className='commentList'>
//             {
//                 commentList.map(item => (
//                     <CommentItem item={item} key={item} />
//                 ))
//             }
//             <div className="page">
//                 <Pagination
//                     defaultCurrent={pageOption.page}
//                     current={pageOption.page}
//                     total={total}
//                     defaultPageSize={pageOption.pageSize}
//                     pageSize={pageOption.pageSize}
//                     onChange={handleCommentListChange}
//                 />
//             </div>
//         </div>
//     );
// }


// export default CommentList;












