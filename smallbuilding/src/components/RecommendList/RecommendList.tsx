import EmptyPage from "../../components/EmptyPage/EmptyPage";
import { useTranslation } from "react-i18next"
import React from "react";
import "./index.scss";
interface PropsTyps {
  str: string;
  children: any;
}

function RecommendList(props: PropsTyps) {
  let { str } = props;
  const {t}:any = useTranslation()
  return (
    <div className="recommend-wrap">
      <div className="recommend-wrap-title">{t(str)}</div>
      <div className="recommend-wrap-list">
        {props.children && props.children.length >= 1 ? (
          props.children.map((item: any, index: number) => {
            if (item.type) {
              return (
                <item.type
                  key={index}
                  style={{
                    animation: `example1 ${(index + 1) * 0.1}s ease-out ${
                      (index + 2) * 0.1
                    }s backwards`,
                  }}
                  onClick={() => {
                    item.props.onClick
                      ? item.props.onClick()
                      : console.log("没有添加点击事件");
                  }}
                >
                  {item.props.children}
                </item.type>
              );
            }
            return <li key={index}>{item}</li>;
          })
        ) : (
          <EmptyPage></EmptyPage>
        )}
      </div>
    </div>
  );
}

export default RecommendList;
