import React from 'react'
import { useState, useEffect, useRef } from "react"
import { Button, Modal } from 'antd';
import { QRCodeSVG } from "qrcode.react";
import { useTranslation } from "react-i18next";
import "./index.scss"
import useCanvas from "../kowledgeCanvas/index"
function Index(props: any) {
    //接收父组件传来的参数
    const { showModal, close, ModalList } = props
    const [isModalOpen, setIsModalOpen] = useState(false);
    const { t } = useTranslation();
    useEffect(() => {
        setIsModalOpen(showModal)
    }, [showModal])

    const handleCancel = () => {
        setIsModalOpen(false);
        close(false)
    };
    const off = () => {
        setIsModalOpen(false)
        close(false)
    }
    console.log(ModalList, "modallist");

    const canvasDom = useRef(null);
    const canvasUrl = useCanvas(ModalList, canvasDom);
    console.log(canvasUrl, "canvasurl");

    return (
        <div>
            <Modal title={t("share")} open={isModalOpen} onCancel={handleCancel} footer={null}>
                <div className="img">
                    {ModalList && ModalList ? (
                        <img className="bullet-imgs" src={ModalList.cover} alt="" />
                    ) : (
                        ""
                    )}
                </div>
                {ModalList && ModalList ? (
                    <div className="bullet-grams-title">
                        <b>{ModalList.title}</b>
                    </div>
                ) : ("")}
                {ModalList && ModalList ? (
                    <div className="bullet-grams-title">
                        <b>{ModalList.summary}</b>
                    </div>
                ) : ("")}
                <div className="bullet-grams-QRCodeSVG">
                    <div className="bullet-grams-QRCodeSVG-svg">
                        <QRCodeSVG value="https://www.npmjs.com/?track=newUserCreated" />
                    </div>
                    <div className="bullet-grams-QRCodeSVG-title">
                        <p>{t("shareNamespace.qrcode")}</p>
                        <p>
                            {t('shareNamespace.shareFrom')} <span>sunshine</span>
                        </p>
                    </div>
                </div>

                <div className="footer">
                    <Button onClick={() => off()} type="primary" danger>关闭</Button>
                    {/* 生成海报*/}
                    <img src={canvasUrl} alt="" />
                    <canvas ref={canvasDom} style={{ display: "none" }} >你的浏览器不支持canvas</canvas>
                    <Button>
                        <a href={canvasUrl} download="canvas.png">下载</a>
                        {/* 下载 */}
                    </Button>
                    <Button onClick={() => off()} type="primary" danger>{t("close")}</Button>
                    <Button>{t("download")}</Button>
                </div>
            </Modal>
        </div>
    )
}

export default Index