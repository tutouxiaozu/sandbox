import React from "react";
import "./list.style.scss";
import { useNavigate } from "react-router-dom";
import BulletFrame from "../BulletFrame/BulletFrame";
import { useTranslation } from "react-i18next";
import {
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined,
} from "@ant-design/icons";

// import PayBulletFrame from "../../components/PayBulletFrame/PayBulletFrame";

interface PropsTypes {
  data: any;
}

function ListItem(props: PropsTypes) {
  let { data } = props;

  const navigate = useNavigate();
  const [Item, setItem] = React.useState(null);
  const [flag, setFalg] = React.useState(false);

  // const [ItemPay, setItemPay] = React.useState(null);
  // const [flagPay, setFalgPay] = React.useState(false);

  const { t } = useTranslation();

  const clone = (flag: any) => {
    setFalg(flag);
  };
  
  // const clonePay = (flag: any) => {
  //   setFalgPay(flag);
  // };

  // 点击 文章 跳转到详情页,但是要支付
  const listClick = (item: any) => {
    // if (!item.isPay) {
    //   setFalgPay(true);
    //   setItemPay(item);
    // }
    navigate("/smallbuilding/articleDetails/" + item.id, {
      state: item,
    })
  };

  return (
    <div className="list-item-wrap">
      {data &&
        data.map((item: any, index: number) => {
          return (
            <div className="list-item-wrap-box" key={index}>
              <div className="list-item-wrap-box-title">
                <b className="b-title">{item.title}</b>
                &emsp;
                <i>✿</i>
                &emsp;
                <span>
                  大约在
                  {Math.floor(
                    (new Date().getTime() -
                      new Date(item.publishAt).getTime()) /
                      (24 * 60 * 2400)
                  )}
                  天前发布
                </span>
                &emsp;
                <i>{item.category != null ? "✿" : ""}</i>
                &emsp;
                <span>{item.category != null ? item.category.label : ""}</span>
              </div>
              <div
                className="list-item-wrap-box-content"
                onClick={() => listClick(item)}
              >
                <div className="list-item-wrap-box-content-right">
                  <div className="list-item-wrap-box-cintent-right-svg">
                    {/* 喜欢,点赞 */}
                    <span
                      onClick={(e) => {
                        e.stopPropagation();
                      }}
                    >
                      <HeartOutlined style={{ color: "red !important" }} />
                      <b className="ListItemNum">{item.likes}</b>
                    </span>
                    <span className="ListItemNum">▪</span>
                    {/* 查看量 */}
                    <span
                      onClick={(e) => {
                        e.stopPropagation();
                      }}
                    >
                      <EyeOutlined />
                      <b className="ListItemNum">{item.views}</b>
                    </span>
                    <span className="ListItemNum">▪</span>
                    {/* 评论 */}
                    <span
                      onClick={(e) => {
                        e.stopPropagation();
                        setItem(item);
                        setFalg(true);
                      }}
                    >
                      <ShareAltOutlined />
                      <b className="ListItemNum">{t("share")}</b>
                    </span>
                  </div>
                </div>
                <div className="list-item-wrap-box-content-left">
                  {item.cover && item.cover ? (
                    <div>
                      <img src={item.cover} alt="" className="cover" />
                    </div>
                  ) : (
                    <div />
                  )}
                </div>
              </div>
            </div>
          );
        })}
      <BulletFrame
        openFrame={flag}
        information={Item}
        clone={clone}
      ></BulletFrame>
      {/* <PayBulletFrame
        Item={ItemPay}
        flag={flagPay}
        clone={clonePay}
      ></PayBulletFrame> */}
    </div>
  );
}

export default ListItem;
