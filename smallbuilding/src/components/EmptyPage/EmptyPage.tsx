import {  Space } from "antd";
import {  LoadingOutlined } from "@ant-design/icons";
import "./index.scss"
// 没有数据的页面展示
function EmptyPage() {
  return (
    <Space className="emptyPage">
      <LoadingOutlined className="IconLoading"  style={{ color: 'hotpink' }} />
    </Space>
  );
}
export default EmptyPage;