import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import "./index.scss";

function TabList(props: any) {
  let { data } = props;
  const navigate = useNavigate()
  const [list, setList] = useState<any>([]);
  const [current, setCurrent] = useState("所有");
  const { t } = useTranslation();
  useEffect(() => {
    let arr: any[] = [];
    arr.push({
      label: t('All'),
      id: "all",
    });
  data.forEach((item: any) => {
      arr.push(item);
    });
    setList(arr);
  }, [data]);

  return (
    <div className="article-wrap-left-list-nav">
      {list &&
        list.map((item: any) => {
          return (
            <span
              key={item.id}
              className={current === item.label ? "action" : ""}
              onClick={() => {
                setCurrent(item.label);
                navigate(`/smallbuilding/category/${item.value}`)
              }}
            >
              {item.label}
            </span>
          );
        })}
    </div>
  );
}

export default TabList;
