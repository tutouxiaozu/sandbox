import I18n from "../../components/I18n/index";
import Style_Switch from "../../components/StyleSwitch";
import { Layout } from "antd";
import { useNavigate } from "react-router-dom";
import { SearchOutlined } from "@ant-design/icons";
import {useTranslation} from "react-i18next"
const { Header } = Layout;
interface PropsType {
  routes: any;
  current: string;
  SearchBtn: Function;
  PcTitleSetCurrent: Function;
}

function PcHeader(props: PropsType) {

  let { routes, current, PcTitleSetCurrent, SearchBtn } = props;

  const navigate = useNavigate();

  const PcSetCurrentChildren = (value: string) => {
    PcTitleSetCurrent(value);
  };
  const { t }: any = useTranslation()
  return (
    <Header>
      {/* 头部导航 */}
      <div className="home-header-nav">
        <div className="home-header-nav-image">
          <img
            src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-09-05/001.gif"
            alt=""
          />
        </div>
        <div className="home-header-nav-tabtitle">
          {/* 头部的导航渲染 */}
          {routes &&
            routes.map((item: any, index: number) => {
              return (
                <span
                  key={index}
                  className={current === item.path ? "action" : ""}
                  onClick={() => {
                    navigate(item.path);
                    PcSetCurrentChildren(item.path);
                  }}
                >
                  {t(item.name)}
                </span>
              );
            })}
        </div>
      </div>
      {/* PC 页面头部 功能盒子 */}
      <div className="home-header-utils">
        <I18n></I18n>
        {/* eslint-disable-next-line react/jsx-pascal-case */}
        <Style_Switch></Style_Switch>
        <SearchOutlined onClick={() => SearchBtn()} />
      </div>
    </Header>
  );
}

export default PcHeader;
