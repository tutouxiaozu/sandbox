import { Input } from "antd";
import store from "../../store";
import { throttle } from "lodash";
import { CloseOutlined } from "@ant-design/icons";
import * as actions from "../../actions/archives";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

const { Search } = Input;

type AppDispatch = typeof store.dispatch;

function Index(props: any) {
  let { show, setShow } = props;

  const [value, setValue] = useState("");
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.get_archives_LIST());
  }, [dispatch]);

  const CloneBtn = () => {
    setShow(false);
  };
  const list = useSelector((state: any) => {
    return state.ArchivesReducer.list;
  });

  let archivesList = JSON.parse(JSON.stringify(list));
  let arr: any = Object.values(archivesList)[0];
  let listitem: any = arr ? Object.values(arr)[0] : "";
  const onSearch = throttle((val: string) => {
    setValue(val);
  }, 300);

  return (
    <div>
      {show ? (
        <div className="site-layout-dialog">
          <div className="site-layout-middleBox">
            <div className="site-layout-headerBox">
              <div className="site-layout-headerBox-left">
                <p>searchArticle</p>
              </div>
              <div className="site-layout-headerBox-right">
                <div
                  className="site-layout-headerBox-right-box"
                  onClick={CloneBtn}
                >
                  <CloseOutlined
                    className="SearchIcon"
                    style={{ fontSize: 20 }}
                  />
                  <p>esc</p>
                </div>
              </div>
            </div>
            <div className="site-layout-MainBox">
              <Search
                placeholder="input search text"
                onSearch={() => onSearch}
                className="SearchInp"
              />
            </div>
            <div className="site-layout-ListBox">
              {value
                ? listitem &&
                  listitem
                    .filter(
                      (data: any) =>
                        !value ||
                        data.title.toLowerCase().includes(value.toLowerCase())
                    )
                    .map((item: any, index: number) => {
                      return <p key={index}>{item.title}</p>;
                    })
                : ""}
            </div>
          </div>
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
}

export default Index;
