import { Modal } from "antd";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

function PayBulletFrame(props: any) {
  const { flag, clone , Item } = props;

  const navigate = useNavigate();

  const [isModalOpen, setIsModalOpen] = useState(false);

  // 点击支付按钮
  const handleOk = () => {
    axios.get("/api/api/payment",{
        params:{
            Item,
            totalAmount:Item&&Item.totalAmount,
            Id:Item.id
        }
    }).then((res) => {

      if (res.data.code === 200) {
        window.location.href = res.data.result;
      }
    });
    // setIsModalOpen(false);
  };

  // 点击取消按钮
  const handleCancel = () => {
    navigate("/");
    setIsModalOpen(false);
    clone(false);
  };

  // 如果 状态发生改变的时候 改变 状态
  React.useEffect(() => {
    setIsModalOpen(flag);
  }, [flag]);

  return (
    <div>
      <Modal
        title="确认以下收费信息"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        okText={"支付"}
        cancelText={"取消"}
      >
        <p>需要支付{Item&&Item.totalAmount}元</p>
      </Modal>
    </div>
  );
}

export default PayBulletFrame;
