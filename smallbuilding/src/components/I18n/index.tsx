import { Menu, Dropdown } from 'antd';
import IconFont from "../Icon/icon"
import i18n from "../../i18next"
import { useTranslation } from "react-i18next"
const I18nBtn = () => {
    const hanldeClickMenu = ({ key }: any) => {
        i18n.changeLanguage(key);
    }
    const { t } = useTranslation();
    const menu = (
        <Menu
            onClick={hanldeClickMenu}
            items={[
                {
                    key: 'cn',
                    label: t('zh')
                },
                {
                    key: 'en',
                    label: t('en')
                }
            ]}
        />
    )
    return (
        <Dropdown overlay={menu}>
            <IconFont type="icon-zhongyingwen"/>
        </Dropdown>
    )
}

export default I18nBtn;