import I18n from "../../components/I18n/index";
import { useNavigate } from "react-router-dom";
import { SearchOutlined } from "@ant-design/icons";
import Style_Switch from "../../components/StyleSwitch";

function MoveBullet(props: any) {
  const navigate = useNavigate();

  const { routes, current, MoveTitleSetCurrent, SearchBtn } = props;

  const MoveBulletPathClick = (state: boolean) => {
    MoveTitleSetCurrent(state);
  };

  return (
    <div className="nav-pop-box">
      {/* 手机端的 顶部 导航 弹框 */}
      {routes &&
        routes.map((item: any, index: number) => {
          return (
            <div
              key={index}
              className={current === item.path ? "action" : ""}
              onClick={() => {
                navigate(item.path);
                MoveBulletPathClick(false);
              }}
            >
              {item.name}
            </div>
          );
        })}
      <div className="phone-utils">
        <I18n></I18n>
      </div>
      <div className="phone-utils">
        {/* eslint-disable-next-line react/jsx-pascal-case */}
        <Style_Switch></Style_Switch>
      </div>
      <div className="phone-utils">
        <SearchOutlined onClick={() => SearchBtn} />
      </div>
    </div>
  );
}

export default MoveBullet;
