import { useState, useEffect } from "react";
import IconFont from "../Icon/icon";
const themes = [
  {
    icon: "icon-taiyang-copy",
    className: "light",
  },
  {
    icon: "icon-yejing",
    className: "dark",
  },
];
type Props = {};

// eslint-disable-next-line no-empty-pattern
function Index({ }: Props) {
  const [themeInex, setThemeIndex] = useState(() => {
    return new Date().getHours() > 6 && new Date().getHours() < 19 ? 0 : 1;
  });

  useEffect(() => {
    document.documentElement.className = themes[themeInex].className;
  }, [themeInex]);

  const hanldClickThemeIcon = () => {
    setThemeIndex((val) => val === 0 ? 1 : 0);
  };

  return (
    <IconFont
      type={themes[themeInex].icon}
      onClick={hanldClickThemeIcon}
      id="iconFont"
    />
  );
}

export default Index;
