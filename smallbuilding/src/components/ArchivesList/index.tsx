import React from 'react';
import { useNavigate } from "react-router-dom";

function ArchivesList({ listitem }:any) {
    const navigate = useNavigate();
  return (
    <div>
          {listitem &&
              listitem.map((item: any, index: number) => {
                  return (
                      <li key={index}
                          style={{ animation: `example1 ${(index + 1) * 0.1}s ease-out ${(index + 2) * 0.1}s backwards` }}
                          onClick={() => {
                              navigate("/smallbuilding/articleDetails/" + item.id, { state: item })
                          }}>
                          {item.publishAt.slice(5, 10)}
                          <span>{item.title}</span>
                      </li>
                  );
              })}
    </div>
  )
}

export default ArchivesList