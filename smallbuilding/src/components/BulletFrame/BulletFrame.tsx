import { Modal } from "antd";
import { useState, useEffect } from "react";
import { useRef } from "react";
import { QRCodeSVG } from "qrcode.react";
// import CanvasPoster from "../CanvasPoster/CanvasPoster";
import useCanvas from "../../hooks/useCanvas";
import "./bulletFrame.scss";
// 生成二维码 弹框组件

interface PropsType {
  openFrame:boolean,
  clone:Function,
  information:any
}

// 生成二维码 弹框组件
function BulletFrame(props: PropsType) {
  // openFrame 状态 控制弹框开关
  // clone 事件 点击弹框中的事件  关闭 弹框
  // information 点击的当前  分享的 数据 ，
  let { openFrame, clone, information } = props;

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [datas, setData] = useState<any>({});

  // 监听 当datas发生改变的时候 重新赋值，调用 getHttp事件
  useEffect(() => {
    setIsModalOpen(openFrame);
    setData(information);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [openFrame, isModalOpen]);

  // 获取 canvas的dom
  const canvasDom = useRef(null); 

  const canvasUrl =  useCanvas(datas, canvasDom);
  
  // 点击下载的时候
  const handleOk = () => {
    // setIsModalOpen(false);
    // console.log("下载");
  };

  // 点击取消的时候
  const handleCancel = () => {
    setIsModalOpen(false);
    clone(false);
  };

  return (
    <div className="bullet-frams-wrap">
      <Modal
        title={'Bullet Title'}
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText={"取消"}
        okText={<a href={canvasUrl} download="canvas.png">下载</a>}
      >
        {isModalOpen && (
          <div>
            <canvas ref={canvasDom} style={{display:'none'}}></canvas>
            <div className="bullet-grams-img">
              {information && information ? (
                <img className="bullet-imgs" src={information.cover} alt="" />
              ) : (
                ""
              )}
            </div>
            {information && information ? (
              <div className="bullet-grams-title">
                <b>{information.title}</b>
              </div>
            ) : (
              ""
            )}
            {information && information ? (
              <div className="bullet-grams-summary">{information.summary}</div>
            ) : (
              ""
            )}
            <canvas ref={canvasDom} style={{display:'none'}}></canvas>
            <div className="bullet-grams-QRCodeSVG">
              <div className="bullet-grams-QRCodeSVG-svg">
                <QRCodeSVG value="https://www.npmjs.com/?track=newUserCreated" />
              </div>
            </div>
          </div>
        )}
      </Modal>
    </div>
  );
}

export default BulletFrame;
