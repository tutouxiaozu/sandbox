import React, { useState, useEffect } from "react";
import { Avatar, Button, Comment, Form, Input, List, Pagination , Space  } from "antd";
import * as http from "../../api/article/action";
// import { LikeOutlined, MessageOutlined, StarOutlined } from '@ant-design/icons';

const { TextArea } = Input;

interface CommentItem {
  author: string;
  avatar: string;
  content: React.ReactNode;
  datetime: string;
}

interface EditorProps {
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
  onSubmit: () => void;
  submitting: boolean;
  value: string;
}

// 评论的 渲染
const CommentList = ({ comments }: { comments: CommentItem[] }) => (
  <List
    dataSource={comments}
    itemLayout="horizontal"
    renderItem={(props) => <Comment {...props} />}
  />
);

// 发布 评论 弹框
const Editor = ({ onChange, onSubmit, submitting, value }: EditorProps) => (
  <>
    <Form.Item>
      <TextArea rows={4} onChange={onChange} value={value} />
    </Form.Item>
    <Form.Item>
      <Button
        htmlType="submit"
        loading={submitting}
        onClick={onSubmit}
        type="primary"
      >
        评论
      </Button>
    </Form.Item>
  </>
);

function CommentBull() {
  // 评论的数据
  const [comments, setComments] = useState<CommentItem[]>([]);
  //
  const [submitting, setSubmitting] = useState(false);
  // 评论输入框的 value值
  const [value, setValue] = useState("");

  useEffect(() => {
    http.GetComment().then((res: any) => {
      let arr: any = res.data.data[0];
      let data: any = [];
      arr.forEach((item: any) => {
        data.push({
          author: <span>{item.name}</span>,
          avatar: "https://joeschmoe.io/api/v1/random",
          content: <p>{item.content}</p>,
          datetime: new Date().toLocaleDateString(),
        });
      });

      setComments(data);
    });
  }, []);

  const handleSubmit = () => {
    if (!value) return;

    setSubmitting(true);

    setTimeout(() => {
      setSubmitting(false);
      setValue("");
      setComments([
        {
          author: "碳烤猪蹄",
          avatar: "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202103%2F05%2F20210305175709_588f3.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1666740483&t=3c0518600a0995e9a7185e40ce77cf5d",
          content: <p>{value}</p>,
          datetime: new Date().toLocaleDateString(),
        },
        ...comments,
      ]);
    }, 1000);
  };

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setValue(e.target.value);
  };

  // 楼层
  const [pageSize, setPageSize] = useState(1);
  const [limit] = useState(3);
  const pagincomments = comments.slice(
    (pageSize - 1) * limit,
    pageSize * limit
  );

  const IconText = ({ icon, text }: any) => (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  );
  console.log(IconText(1));
  

  return (
    <>
      <Comment
        avatar={
          <Avatar src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202103%2F05%2F20210305175709_588f3.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1666740483&t=3c0518600a0995e9a7185e40ce77cf5d" alt="Han Solo" />
        }
        content={
          <Editor
            onChange={handleChange}
            onSubmit={handleSubmit}
            submitting={submitting}
            value={value}
          />
        }
      />
      {/* 渲染评论 */}
      {comments.length > 0 && <CommentList comments={pagincomments} />}
      {/* 评论的分页 */}
      <Pagination
        defaultCurrent={1}
        total={comments.length}
        onChange={(e: any) => setPageSize(e)}
        pageSize={limit}
        current={pageSize}
      />
    </>
  );
}

export default CommentBull;
