import { useState,useEffect} from 'react'

//监测图片是否加载完成
const getImg=(url:string)=>{
    return new Promise((resolve, reject) => {
        //实例化img
        const img=new Image();
        img.crossOrigin="anonymous";//允许跨域
        img.onload=()=>{
            resolve(img);
        }
        img.onerror=(error)=>{
            reject(error);
        }
        img.src=url;
    })
}

//绘图
const drawUrl=async (ctx:any,ModalList:any,canvasDom:any)=>{
  //绘制填充背景
  ctx.fillStyle="#fff";
  ctx.fillRect(0,0,400,679)
  //绘制标题
  ctx.font="30px lightblue";
  ctx.fillStyle="#000";
  ctx.fillText(ModalList.title,124,30)

  //绘制标题下面的线
  ctx.beginPath();
  ctx.strokeStyle="blue";
  ctx.moveTo(0,40);
  ctx.lineTo(400,40);
  //开始绘制
  ctx.stroke();

  //绘制图片
  const img=await getImg(ModalList.cover);
  ctx.drawImage(img, 50, 100, 200, 200);
  console.log(img);
  console.log(ctx);
  //  绘制下面的文字
  ctx.font = '12px yellow';
  ctx.fillStyle = '#000';
  ctx.fillText(ModalList.title, 50, 450);
  ctx.fillText(ModalList.summary, 50, 470);

  //  生成url 把canvas转成图片
  const imgUrl = canvasDom.current.toDataURL();
  console.dir(imgUrl, 'img');
  return imgUrl;

}
//生成canvas
const createCanvas=async (ModalList:any,canvasDom:any)=>{
    //设置canvas的宽高
    canvasDom.current.width=400;
    canvasDom.current.height=679;
    //创建上下文对象
    const ctx=canvasDom.current.getContext("2d");
    //绘制
    let imgUrl=await drawUrl(ctx,ModalList,canvasDom);
    return imgUrl;
}
function Index(ModalList:any,canvasDom:any) {
  const [url,setUrl]=useState('')
  useEffect(()=>{
  (async ()=>{
    let url=await createCanvas(ModalList,canvasDom);
    setUrl(url);
  })()
  },[ModalList])
  return (
    // <div>index</div>
    // "123"
    url
  )
}

export default Index