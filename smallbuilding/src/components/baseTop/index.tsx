import React from 'react'
import { BackTop } from 'antd';
import "./index.scss"

function BaseTop() {
  // console.log(visibilityHeight)
  return (
      <>
          <BackTop visibilityHeight={200} />
          <strong className="site-back-top-basic"> </strong>
      </>
  )
}

export default BaseTop