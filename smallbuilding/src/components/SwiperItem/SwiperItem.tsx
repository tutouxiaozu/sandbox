import "./swiperItem.scss";
import { Carousel } from "antd";
import { ActicleListType } from "../../types/store/ArticleReducer/acticeReducer.d";

interface PropsType {
  data: ActicleListType[]
}


function SwoperItem(props: PropsType) {
  let { data } = props;
  return (
    <div className="swiper-item">
      <Carousel autoplay>
        {data &&
          data.map((item: ActicleListType, index: number) => {
            return (  
              <div
                className="swiper-item-box"
                key={index}
                // style={{ background : item.cover }}
              >
                {" "}
                <img src={item.cover} alt="" />
                <div className="swiper-item-box-content">
                  <h2 className="swiper-item-box-content-title">
                    {item.title}
                  </h2>
                </div>
              </div>
            );
          })}
      </Carousel>
    </div>
  );
}

export default SwoperItem;
