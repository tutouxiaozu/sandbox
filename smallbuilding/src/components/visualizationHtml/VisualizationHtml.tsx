function VisualizationHtml(Com: any) {
  return (props: any) => {
    let flag = true;
    // let WindowClient = () => {
    let clientW = document.documentElement.clientWidth;
    //   if (
    //     document.documentElement.clientWidth >=
    //     770
    //   ) {
    //     flag = true;
    //   } else {
    //     flag = false;
    //   }
    // };

    window.onresize = function () {
    //   if (
    //     document.documentElement.clientWidth >=
    //     770
    //   ) {
    //     flag = true;
    //   } else {
    //     flag = false;
    //   }
        clientW = document.documentElement.clientWidth;
    };
    return <Com {...props} {...{ flag, clientW }}></Com>;

    // if (flag) {
    //   return <Com {...props} {...{ flag, WindowClient }}></Com>;
    // } else {
    //   return <Com {...props} {...{ flag, WindowClient }}></Com>;
    // }

  };
}

export default VisualizationHtml;
