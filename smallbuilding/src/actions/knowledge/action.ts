// import request from "../../utils/request";
import {Dispatch} from "redux"
import {getKnowledge} from "../../api/knowledge/action"
export const get_Knowledge=()=>{
    return async (dispatch:Dispatch)=>{
        const {data}=await getKnowledge();
        console.log(data,"k");
        
        dispatch({
            type:"GET_KNOWLEDGE",
            payload:data.data[0]
        })
    }
}