import { get_archives_list, get_archives_rightTop_list, get_archives_rightBottom_list } from "../api/archives/index"
import { Dispatch } from "redux"

export const get_archives_LIST = () => {
    return async (dispatch: Dispatch) => {
        const data = await get_archives_list();
        dispatch({
            type: "GET_ARCHIVES_LIST",
            payload: data.data,
        })
    }
}
export const get_archives_right_LIST = () => {
    return async (dispatch: Dispatch) => {
        const data = await get_archives_rightTop_list();
        dispatch({
            type: "GET_ARCHIVES_RIGHTTOP",
            payload: data.data,
        })
    }
}
export const get_archives_bottom_LIST = () => {
    return async (dispatch: Dispatch) => {
        const data = await get_archives_rightBottom_list();
        dispatch({
            type: "GET_ARCHIVES_BOTTOM",
            payload: data.data,
        })
    }
}