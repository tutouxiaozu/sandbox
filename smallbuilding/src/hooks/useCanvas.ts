import { useEffect, useState } from "react";
// import QRCode from "qrcode";

// 首先 检测一下图片是否加载完成 的方法
const GetImg = (url: any) => {
  return new Promise((resolve, reject) => {
    // 异步加载 图片
    const img = new Image();
    // 配置允许跨域
    img.crossOrigin = "anonymous";
    // 图片加载成功 返回图片的实例
    img.onload = () => {
      resolve(img);
    };
    img.onerror = (error) => {
      reject(error);
    };

    // 成功的话 返回 正确路径的图片，否则返回错误
    img.src = url;
  });
};

// const GetQRcodeSvg = async (data: any) => {
//   // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
//   let QRCodeUrl: any = '';
//   const img = new Image()
//   QRCode.toDataURL(data, (err, url) => {
//     QRCodeUrl = url
//   })
//   return img.src = QRCodeUrl;
// }


// 配置 画布 的样式
const convasStyle = async (ctx: any, data: any, canvasDom: any) => {
  // 绘制 画布的背景颜色
  ctx.fillStyle = "white";
  // ctx.fillStyle = "hotpink";
  ctx.fillRect(0, 0, 400, 979);

  //  绘制标题
  ctx.font = "25px hotpink";
  ctx.fillStyle = "#000";
  ctx.fillText(data.title, 30, 30);

  //  绘制标题下面的线
  ctx.beginPath();
  ctx.strokeStyle = "#999";
  ctx.moveTo(0, 40);
  ctx.lineTo(400, 40);
  ctx.stroke();  //  开始绘制

  //  绘制图片
  const img = await GetImg(data.cover);
  ctx.drawImage(img, 0, 70, 370, 300);

  
  //  绘制下面的文字
  ctx.font = "16px yellow";
  ctx.fillStyle = "#333";
  ctx.fillText(data.title, 10, 400);
  ctx.fillText(new Date().toLocaleDateString(), 10, 440);

  //  绘制二维码
  // const QrCode = await GetQRcodeSvg(data.cover);
  // ctx.drawImage(QrCode, 100, 360, 100, 100);


  //  生成url 把canvas转成图片
  const imgUrl = canvasDom.current.toDataURL();
  return imgUrl;
};

const createConvas = async (data: any, canvasDom: any) => {
  //  设置canvas的宽高
  canvasDom.current.width = 350;
  canvasDom.current.height = 500;
  //  创建上下文对象
  const ctx = canvasDom.current.getContext("2d");

  //  绘制
  let imgUrl = await convasStyle(ctx, data, canvasDom);
  return imgUrl;
};

function useCanvas(data: any, canvasDom: any) {

  const [url, setUrl] = useState("");
  useEffect(() => {
    (async () => {
      let url: any = await createConvas(data, canvasDom);
      setUrl(url);
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);
  return url;
}

export default useCanvas;
