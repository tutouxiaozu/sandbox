import { get_list, get_lists } from "../api/article/action"
import { Dispatch } from "redux"
// import { ListItem } from "../types/store/AdminReducer/adminReducer"
export const get_list_action = () => {
    return async (dispatch: Dispatch) => {
        const { data } = await get_list()
        dispatch({
            type: "GET_LIST",
            payload: data.data
        })
    }
}
export const get_lists_action = () => {
    return async (dispatch: Dispatch) => {
        const { data } = await get_lists()
        dispatch({
            type: "GET_LISTS",
            payload: data.data
        })
    }
}
// export const deng_xin = (item: ListItem) => {
//     return {
//         type: "DENG_XIN",
//         payload: item
//     }
// }



