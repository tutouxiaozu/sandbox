
// 首先引入 依赖
import i18n from 'i18next';

// 需要在 react中使用
import { initReactI18next } from 'react-i18next';

import LanguageDetector from 'i18next-browser-languagedetector';

import cn from "./config/ch.config";

import en from "./config/en.config";

// 定义 语言的资源
const resource = {
    en: { translation: en }, // 英文
    cn: { translation: cn }, // 中文
}

// 初始化国际化
// 先加载一下语言的资源
i18n.use(initReactI18next)
    .use(LanguageDetector)
    .init({
        resources: resource,
        detection: { // 检查语言
            order: [
                'querystring',
                'cookie',
                'localStoreage',
                'sessionStorage',
                'navigator',
                'htmlTag',
                'path',
                'subdomain',
            ]
        },
        fallbackLng: "cn",
        interpolation: {
            escapeValue: false,
        }
    })

export default i18n