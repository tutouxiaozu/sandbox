import request from "../../utils/request";
// 获取数据列表
export const get_archives_list = () => request.get("/api/article/archives");

export const get_archives_rightTop_list = () => request.get("/api/article/recommend");

export const get_archives_rightBottom_list = () => request.get("/api/category");