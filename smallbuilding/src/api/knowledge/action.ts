import request from "../../utils/request";

//知识小册页面的接口
export const getKnowledge=()=>request.get("/api/Knowledge")
//知识小册页面categoryTitle部分
// export const getCategoryTitle=()=>request.get("/api/article/category/{id}")
export const rightTop = () => request.get("/api/article/recommend");
export const rightBottom= () => request.get("/api/category");
