
import axios from '../../utils/request'

//获取tag数据列表
export const get_taglsit = async () => await axios.get('/api/tag')

//获取全部的数据
export const GetTagList = async () => await axios.get('/api/article')

export const GetTagVal = async (val) => await axios.get(`/api/article/tag/${val}`)

//右侧上方的数据
export const GetTuiList = async () => await axios.get("/api/article/recommend")

// 测试
export const CheShi = async (val) => await axios.get(`/api/tag/${val}`)

