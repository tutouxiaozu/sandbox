import axios from "../../utils/request"
export const get_list = () => axios.get("/api/article/all/recommend")
export const get_lists = () => axios.get("/api/article/all/recommend")
export const GetComment = () => axios.get("/api/comment")
export const GetHtml = async (id) => axios.get(`/api/article/${id}`)
export const GetToc = async (id) => axios.get(`/api/article/${id}`)
export const postComment = (params) => axios.post(`/api/comment`, params)

// export const GetToc = async (id) => axios.get(`/api/article/${id}`)
// export const GetHtml = () => axios.get(`/api/article/all/recommend`)
