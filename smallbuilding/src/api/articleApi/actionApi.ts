import request from "../../utils/request";

// 获取 tag 标签的数据
export const GetActicleTags = async () => await request.get('/api/tag');

// 文章列表获取数据
export const GetActicleList = async () => await request.get('/api/article');

// 文章所有的  tabNav 接口
export const GetAllCategoryList = async () => await request.get('/api/category');

// 文章页面的 右侧 推荐数据接口
export const ActicleRecommend = async () => await request.get('/api/article/recommend');

// 文章 all 所有的数据
export const GetAllTabActicle = async () => await request.get('/api/article/all/recommend');

// 作者 详情页面 获取数据的接口
export const GetCategory = async (val: string | undefined) => await request.get(`api/category/${val}`);

// 作者详情页面 获取当前作者的信息数据
export const GetCategoryList = async (val: string | undefined) => await request.get(`/api/article/category/${val}`)

// NICKWORk 测试
export const CESHI = async (id: string | undefined) => await request.get(`/api/knowledge/${id}`)
