import React, { useState } from "react";
//引入i18n
import i18next from "./i18n";
//切换语言的
import { useTranslation } from 'react-i18next'

// import {useState} from 'react'
const Note = () => {
    const [lang, setLang] = useState(() => navigator.language)      //zh-cn中文
    console.log(lang);
    const { t } = useTranslation()
    return (
        <div>
            国际化
            <p>
                {t('文章')}
            </p>
            <button onClick={() => {
                const newLang = lang === ' en' ? 'zh-CN' : 'en'
                setLang(newLang)
                //调用i18n的changeLanguage方法
                i18next.changeLanguage(newLang)
            }}>切换中英文{lang} </button>
        </div>
    )
}

export default Note