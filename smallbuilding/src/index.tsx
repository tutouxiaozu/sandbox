import ReactDOM from 'react-dom/client';
import './components/style/theme.scss';
import App from './router/RouterView';
import {Provider} from 'react-redux';
import store from './store/index';
import './css/index.css';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);