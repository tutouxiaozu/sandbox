export const initialState = {
    knowledgeList: JSON.parse(localStorage.getItem("knowledgeList") as any) || [],
}

const KnowledgeReducer = (state = initialState, { type, payload }: any) => {
    console.log(payload);
    switch (type) {
        case "GET_KNOWLEDGE":
            localStorage.setItem("knowledgeList", JSON.stringify([...payload]));
            return {
                ...state,
                knowledgeList: [...payload]
            }
        default:
            return {
                ...state
            }
    }
}
export default KnowledgeReducer