
const initialState = {
    list: [],
}

export default (state = initialState, { type, payload }: any) => {
    const newState = JSON.parse(JSON.stringify(state))
    switch (type) {
        case "GET_ARCHIVES_LIST":
            console.log(payload);
            //   if (payload.statusCode ==200){
            // console.log(payload.data[2022].September, 'payload')
            newState.list = payload.data;
            return newState

        default:
            return state
    }
}
