
import { Actions, State } from "../../types/store/AdminReducer/adminReducer"

const initialState: State = {
  listItem: [],
  listsItem: []
}
const UserReducer = (state = initialState, { type, payload }: Actions) => {
  switch (type) {

    case 'GET_LIST':
      return {
        ...state,
        listItem: [...payload]
      }

    default:
      return {
        ...state,
      }
  }
}

export default UserReducer;


