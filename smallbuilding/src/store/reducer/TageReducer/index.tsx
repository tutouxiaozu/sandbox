import { tagList } from "../../../types/store/TagTitle/type";
// tag页面

const initialState = {
  taglist: [],
  everyonclik: [],
};

const TageReducer = (state = initialState, { type, payload }: any) => {
  let newState = JSON.parse(JSON.stringify(state));
  switch (type) {
    case "GET_TAG_LIST":
      newState.taglist = payload;

      return newState;
    //点击每个数据
    case "EVERY_ONCLK":
      newState.everyonclik = newState.taglist.filter(
        (item: tagList) => item.id === payload
      );

      return newState;

    default:
      return newState;
  }
};

export default TageReducer;
