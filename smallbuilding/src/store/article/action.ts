import * as Api from "../../api/articleApi/actionApi";
import { Dispatch } from "redux";

// 文章列表获取数据
export const GetActicleList = () => {
    return async (dispatch: Dispatch) => {
        await Api.GetActicleList().then(res => {
            dispatch({
                type: 'GetActicleList',
                payload: res.data.data[0]
            })
        })
    }
}

