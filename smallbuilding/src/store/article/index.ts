
import {
  AllActicleActionType,
} from "../../types/store/ArticleReducer/acticeReducer.d";

const initialState = {
    acticleList:[],
}

const ArticleReducer = (state = initialState, { type, payload }:AllActicleActionType) => {
    const newState = JSON.parse(JSON.stringify(state));
  switch (type) {

    // 获取文章数据
  case 'GetActicleList':
      newState.acticleList = payload;
    return newState

  default:
    return newState
  }
}

export default ArticleReducer