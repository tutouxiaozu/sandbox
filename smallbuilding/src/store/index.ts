import {
  legacy_createStore as createStore,
  combineReducers,
  applyMiddleware,
} from "redux";

import logger from "redux-logger";
import thunk from "redux-thunk";

import KnowledgeReducer from "./knowledge/index";
import UserReducer from "./reducer/UserReducer/index";
import TageReducer from "./reducer/TageReducer/index";
import ArchivesReducer from "./archives/index";
import ArticleReducer from "./article/index";
import Details from "./Details/index"

const AllStore = combineReducers({
  KnowledgeReducer,
  ArchivesReducer,
  ArticleReducer,
  UserReducer,
  TageReducer,
  Details
});

export default createStore(AllStore, applyMiddleware(logger, thunk));
