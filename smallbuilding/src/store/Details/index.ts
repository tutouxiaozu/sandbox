
import { Actions } from "../../types/store/AdminReducer/adminReducer"

const initialState: any = {
  listItem: [],
  listsItem: []

}
const Details = (state = initialState, { type, payload }: Actions) => {
  switch (type) {

    case 'GET_LIST':
      return {
        ...state,
        listItem: [...payload]
      }
    case 'GET_LISTS':
      return {
        ...state,
        listsItem: [...payload]
      }
    case "DENG_XIN":
      state.listItem.forEach((item: { id: any; flag: boolean }) => {
        if (item.id === payload.id) {
          item.flag = !item.flag
        }
      })
      return {
        ...state,
        listItem: [...state.listItem]
      }
    default:
      return {
        ...state,
      }
  }
}

export default Details;
