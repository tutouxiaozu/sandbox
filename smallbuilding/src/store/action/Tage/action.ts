// import axios from "axios";
import { Dispatch } from "redux";
import { get_taglsit } from "../../../api/Tag/tag";

export const taglist = () => {
  return async (dispatch: Dispatch) => {
    const data = await get_taglsit();
    dispatch({
      type: "GET_TAG_LIST",
      payload: data.data.data,
    });
  };
};

//每一个中间的数据进行点击
export const everyonclik = (id: string) => {
  return {
    type: "EVERY_ONCLK",
    payload: id,
  };
};
