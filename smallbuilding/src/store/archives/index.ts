
const initialState = {
    list: [],
    data: {},
    Right_Top_list: [],
    Bottom_list: [],
}

const ArchivesReducer = (state = initialState, { type, payload }: any) => {
    const newState = JSON.parse(JSON.stringify(state))
    switch (type) {
        case "GET_ARCHIVES_LIST":
            newState.list = payload.data;
            return newState;
        case "GOTO_DETAIL":
            newState.data = payload;
            return newState;
        case "GET_ARCHIVES_RIGHTTOP":
            console.log(payload)
            newState.Right_Top_list = payload.data;
            return newState;
        case "GET_ARCHIVES_BOTTOM":
            newState.Bottom_list = payload.data;
            return newState;
        default:
            return newState;
    }
}

export default ArchivesReducer
