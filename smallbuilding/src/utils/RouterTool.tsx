/*
    存放一些路由使用的方法 的高阶组件;
*/
import { ComponentType } from "react";
import { useNavigate, useLocation, useParams } from "react-router-dom";


function routerTool(Com: ComponentType) {
  return (props: any) => {
    const paramsId = useParams(); // 获取动态路由的 ID
    const location = useLocation(); // 获取路由的参数 state
    const navigate = useNavigate(); // 跳转路由使用

    return <Com {...props} {...{paramsId,location,navigate}}></Com>;
  };
}

export default routerTool;
