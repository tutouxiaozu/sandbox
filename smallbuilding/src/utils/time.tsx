import dayjs from "dayjs"
import relativeTime from "dayjs/plugin/relativeTime"
import updateLocale from "dayjs/plugin/updateLocale"
import "dayjs/locale/zh-cn"
dayjs.locale('zh-cn')
dayjs.extend(updateLocale)
dayjs.extend(relativeTime)

const time = (createTime: string | number | dayjs.Dayjs | Date | null | undefined) => `${dayjs(createTime).fromNow()}`;

export default time;