/* eslint-disable react-hooks/rules-of-hooks */
import {useNavigate} from "react-router-dom";
function HocRequire(Com:any) {
  return (props:any) => {
    const navigate = useNavigate();
    const token = window.localStorage.getItem('token')
    if (token) <Com {...props}></Com>
    else navigate('/login')
  }
}

export default HocRequire