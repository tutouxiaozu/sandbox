module.exports = {
    extends: [
        "react-app",
        "react-app/jest",
        "prettier"
    ],
    // 使用的工具
    plugins: [
        "prettier"
    ],
    rules: {
        // 用来约束 
        // "prettier/prettier": [
        //     "error",
        //     // 对我们代码校验失败的时候,返回一些信息
        //     {
        //         singleQuote: true, // 是否是单引号 ！
        //         printWidth: 100, // 打印宽度
        //         tabWidth: 100, // 标签宽度
        //         trailingComma: "es5", // 尾随逗号
        //         endOfLine: false, // 行结束的时候
        //         semi: false
        //     }
        // ],
        // 如果是生产环境 
        "no-console": process.env.NODE_ENV === "development" ? "off" : "off", // 生产环境 拒绝打印
        // 配置 eslink 限制的规则
        "no-unused-vars": "error",
        // 配置 开发环境
        "no-debugger": process.env.NODE_ENV === "development" ? "off" : "off", // 调试  断点
    }
}

// 如何 使用 prettier 可以美化 ts ,js ,css , html 
