
const Koa = require("koa");

const Router = require("koa-router");

const app = new Koa();
const router = new Router();

const AlipaySdk = require("alipay-sdk").default;
const AlipayFormData = require("alipay-sdk/lib/form").default;

// 配置 AllpaySDK
const alipaySdk = new AlipaySdk({
    appId: '2021000121669880', // 开放平台上创建应用时生成的 appId
    signType: 'RSA2', // 签名算法,默认 RSA2
    gateway: 'https://openapi.alipaydev.com/gateway.do', // 支付宝网关地址 ，沙箱环境下使用时需要修改
    alipayPublicKey: `MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
    CgKCAQEAg86Zo/Pf+HDFC/5RHDjAK39qEIepTVoPm+lo47sgkY3Iqoy
    C/lZy7jGSfr7XGJFIYh2ONML6Z59sLCsTUc4FGXeprsezaUTKfk+
    kqMTTMbgni0hvS5gD+s79kIowzHxVFy2bXNmB6sNPAd7r/rf27EDL
    cpmtm+ujrCl+eTUPMj7D0YXkqf9mkjYIKNhbCTiTB16/HEoQjfVxc
    SjRtYi2iyv/HNzgtS61Ge2ArAOyIm3FB2IX7ULaIqDCwzLOtYM5Rpl+
    cW0S0UT8R+HCPHVBDp/hWKubujqNmVaVKjSdXXzaDX0X/MV85MfQ65W
    6vnzEOM7lwrmt8ijfdRoG0VwUcwIDAQAB`, // 支付宝公钥，需要对结果验签时候必填
    privateKey: `MIIEowIBAAKCAQEAiYrEFlhhrEoTgW0xiurgLyVhfeL
    7Hnk6fuTTYwDpayHeb53qXjOjgj7iqACg0xvvnTrSdtL9B6zZ34aPqxH
    3InZIv5f0hcdLPcbPgZs389ByS1s8r2stbBveT9dDlVTOn/tYrpR2hKxP
    mH9cowukSmhNcIbZc9WxV2g+R34ylbr64yLjw7gTK+J/vgD93w47Wcfl8S
    aLqj1O1O/9nTBElDBZ/hMcgXu+WmVON6eUjvZJIEQwosJX6X7UyR1wOrUb
    76P31qWCayxo31nhaSdPM1SjAJnPsW06gv5kLfyjy7QPdB1JLPDsoaxpxGkW
    XgQySpA3sSWmHxtdDgW/cY9DOwIDAQABAoIBAFBJFwLwHjuClfYztfqf9bK
    vgEQKbYkxPGr7yUR+MMJfZ8cBh9/rBqOtb4Kfs0DniqvdvSKtMBP92sO2eU
    BjYT3Vi8uCuLIqTgpGdbPy1etVHpJMNJU027PCZLd4MTQG6AHMpsxpTvFM37
    1lgr6Nuh3Q2ns3y9DoyPb7m3cB/CcXuEwiEP+JJ3ODRzlvVDhjBPMz9PwzxuJ
    psvrYQ3mHlhJ1kuK/FlrnBo4n8T/FI7HL+cO6yb8mpY7YBMgAjBB8T/WW5JFw
    nIfNlpyLlTbDFoAOvYD5OVF1arEDETKimJ3X9OmBgGxYdoItec9ckMG64B5CUj
    PZhw5Oa1rQ5ooOZykCgYEA61WPaUM1/5xK0ZMYMT0JNPW5KiDJyEDHTBjplFGA
    JMCJXm1fm4C9xUxgYnCxyddg0PEI45gWfAzGRvkNMppyROyCVKIcl5Ku9fIKbM
    JyEC3fY2VqP3e995jOV3HYjqYmpzmTtjnXiqyoX87cgiW7aWq5omQFWeUf0hnkO
    D16BDUCgYEAlZ7JCjbGfPfCz8bEfcAQl9NAWmRDLfJA7Xh2LYm97P6Yx7NaKng0N
    2AjqvDh658uOq5OzE15ar7AJSaWhUosaz73CR1OUPfw+4B2+SEPswZp0Jvln0p/HK
    W+g1FpowvWju6FW8uyorlbdmIMtyLrX3m5GG9wAYusr/Hw5T0jN68CgYEAvqgfurD4A
    3E08FtSYznGxsA41legAnvwuscIHeB1h9Dwua60oYAIvtpkz/vrHdI/HqfYgsMJnFChJ
    ojJV3DnGM3fvP9lWKs+0gTYy0oxLMhtTdKw5GB3aCvFJ/UU9GTJtfsgn6Z26GN/ObeW0F
    4PWpVbdaPA7haB1C6jxaiQvO0CgYAScSX6mlHQ6YEbtzZVAa2J6UIPYjhdm3ShUK3QYyAD
    7EXMp7C/d12ITdNXqKVQM6ixFvcje5ExtR1d9umquD/NYE920iPrGwSbG3SOtKKIwIueaPj
    er4IHd+6JpSlOLZFewMuzSuJT6GD7sI48rJMyi4BjIKk/p4W7kijNOMH0hQKBgEMyuCR3/mu
    LfrJNzm1uiEqMXqVtYEqKk+vqTAAtiinY1vRguRuaRXe2DD07qjr0eI1027t0SHkmlx9ZW/FP
    nuBHA9WBcMyN+SxBy4eqvSPSGNIa7zCYxU16fRMW/zxGgp4bSaT8mxPm7Tti0uuFC6IDhA130h
    I3IwlcfIYGlt1j`, // 应用私钥字符串
});

router.get('/api/payment', async ctx => {
    let { Item,totalAmount ,Id} = ctx.query;
    console.log(Id,'------------===--------------------');

    const formData = new AlipayFormData();
    formData.setMethod('get');
    formData.addField('notifyUrl', 'https://www.baidu.com');
    formData.addField('bizContent', {
        outTradeNo: `12253sdadfff3355`, // 商户订单号,64个字符以内、可包含字母、数字、下划线,且不能重复
        productCode: 'FAST_INSTANT_TRADE_PAY', // 销售产品码，与支付宝签约的产品码名称,仅支持FAST_INSTANT_TRADE_PAY
        totalAmount: `${totalAmount}`, // 订单总金额，单位为元，精确到小数点后两位
        subject: `${Item.title}`, // 订单标题
        body: '商品详情', // 订单描述

    });
    formData.addField('returnUrl', `http://10.37.11.29:3000/`)
    // formData.addField('returnUrl', `http://10.37.11.29:3000/smallbuilding/articleDetails/${Id}`)
    const result = await alipaySdk.exec(  // result 为可以跳转到支付链接的 url
        'alipay.trade.page.pay', // 统一收单下单并支付页面接口
        {}, // api 请求的参数（包含“公共请求参数”和“业务参数”）
        { formData: formData },
    );

    ctx.body = {
        code:200,
        message:'支付',
        result:result
    }

})

app.use(router.routes())

app.listen(9000, () => {
    console.log('服务启动成功   http://localhost:9000');
})